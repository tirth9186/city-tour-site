import React, { Component } from "react";
import "./TourList.scss";
import Tour from "../Tour/Tour";
import { tourData } from "../tourData";
export default class TourList extends Component {
  state = {
    tours: tourData
  };
  remove = id => {
    const temptours = this.state.tours.filter(item => item.id != id);
    this.setState({
      tours: temptours
    });
  };
  render() {
    const { tours } = this.state;
    return (
      <div className="tourlist">
        {tours.map(tour => {
          return <Tour key={tour.id} tour={tour} remove={this.remove} />;
        })}
      </div>
    );
  }
}
