import React, { Component } from "react";
import "./Tour.scss";
export default class Tour extends Component {
  state = {
    showinfo: false
  };
  handleInfo = () => {
    this.setState({
      showinfo: !this.state.showinfo
    });
  };
  render() {
    const { id, img, name, city, info } = this.props.tour;
    const { remove } = this.props;
    return (
      <article className="tour">
        <div className="img-container">
          <img src={img} />
          <span
            className="close-btn"
            onClick={() => {
              remove(id);
            }}
          >
            <i className="fas fa-window-close" />
          </span>
        </div>
        <div className="tour-info">
          <h3>{city}</h3>
          <h4>{name}</h4>
          <h5>
            info&nbsp;
            <span onClick={this.handleInfo}>
              <i className="fas fa-caret-square-down " />
            </span>
          </h5>
          {this.state.showinfo && <p> {info} </p>}
        </div>
      </article>
    );
  }
}
